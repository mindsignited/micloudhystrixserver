# Hystrix Dashboard

---
TO MAKE THINGS EASIER I ADDED A START UP SCRIPT THAT CAN BE USED FOR DEVELOPMENT.
You can run the ```./start.sh``` script and if you want to debug use ```./start.sh debug```.

IF YOU WANT TO DEBUG:
    In Intellij open the toolbar and select Run --> Edit Configurations 
    Then you will want to add a new configuration, select + --> Remote
    In the new Remote window you can name the configuration something useful
    Next under Settings, set Transport: Socket, Debugger Mode: Attach, Host: localhost, Port: 5007 (port the start.sh script uses)

---

Run this app as a normal Spring Boot app and then go to the home page
in a browser. If you run from this project it will be on port 7979
(per the `application.yml`). On the home page is a form where you can
enter the URL for an event stream to monitor, for example (the
customers service running locally):
`http://localhost:7979/hystrix.stream`. Any app that uses
`@EnableHystrix` will expose the stream.

To aggregate many streams together you can use the
[Turbine sample](https://github.com/spring-cloud-samples/turbine).


To run this project you will need to ensure these environment variables are present:

The Eureka Server credentials, these are required.  We could use encrypted fields in configuration file
passed down from the config server but that means they are static until an update.  This way we can change the
creds really easily. All services will only ever need to have these requirements - since eureka is our service discovery.

    export EUREKA_SERVER_USER=user
    export EUREKA_SERVER_PASS=mindsignited
    
    or
    
    export EUREKA_SERVER_URI=http://user:pass@domain:port
    
You can also provide a plain text Hystrix password, this is OPTIONAL - defaults to what is defined below but encrypted:

    export HYSTRIX_SERVER_PASS=mindsignited
    
    
For a quick run from the commandline you can run this command: 

    EUREKA_SERVER_USER=user EUREKA_SERVER_PASS=mindsignited mvn spring-boot:run