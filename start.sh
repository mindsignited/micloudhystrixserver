#!/bin/bash

## MI Cloud Hystrix Server

export SERVER_PORT=7979
export MGMT_SERVER_PORT=7980
export GIT_BRANCH=develop # should be master or another branch in production.
export EUREKA_SERVER_URI=http://localhost:8761/micloudeureka

export KEYCLOAK_BASE_URL=http://127.0.0.1:8080/auth
export KEYCLOAK_REALM=micloud

if [ "$1" == "debug" ]; then
    mvn spring-boot:run -Drun.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5007" --debug
else
    mvn spring-boot:run $@
fi
